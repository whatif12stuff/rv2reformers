﻿using RimWorld.Planet;
using RV2_Reformers.ReformLocationUtil;
using RV2_Reformers.Reform.ScheduledReforms;
using RV2_Reformers.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;
using RimVore2;
using RV2_Reformers.Debug;

namespace RV2_Reformers.Reform.AutoReform
{
    public class ScheduleReformationManager : WorldComponent
    {
        private List<ScheduledReformation> scheduledReformations = new List<ScheduledReformation>();
        public ScheduleReformationManager(World world) : base(world)
        {
        }

        public override void WorldComponentTick()
        {
            base.WorldComponentTick();
            if (!scheduledReformations.Any()) return;

            foreach (var scheduledRecord in scheduledReformations)
            {
                scheduledRecord.TicksTillReform--;
                if (scheduledRecord.IsInvalid)
                {
                    if(scheduledRecord.Pawn == null)
                    {
                        RV2Log.Error("Invalid schedule reform due to null pawn", LogCategories.ScheduleReform);
                        continue;
                    } else
                    {
                        RV2Log.Warning($"Invalid schedule reform record detected for {scheduledRecord.Pawn.Label}. Alive? {(!scheduledRecord.Pawn.Dead)}", LogCategories.ScheduleReform);
                        continue;
                    }
                }
                if (!scheduledRecord.ShouldReform) continue;
                
                TryReform(scheduledRecord);
            }

            scheduledReformations.RemoveAll(item => item.IsInvalid || item.ShouldReform);
        }

        private void TryReform(ScheduledReformation scheduledRecord)
        {
            var record = GlobalReformTracker.GetRecord(scheduledRecord.Pawn);
            if (record == null) return;
            if (AtAssginedSpot()) return;
            switch (scheduledRecord.RefomationLocation)
            {
                case ScheduledReformLocationTargetType.AtDisposal:
                    if (AtDisposal()) return;
                    if (AtCorpse()) return;
                    break;
            }
            if (Fallback()) return;
            RV2Log.Error($"Attempted to complete schedule reformation for {scheduledRecord.Pawn.Label} but fallback failed.", LogCategories.ScheduleReform);

            bool AtAssginedSpot()
            {
                var target = Rv2ReformersFind.ReformLocationUtilComponent.GetAssignedSpot(record.Pawn);
                if(target == null) return false;
                if(target.Value.Map == null) return false;
                return ReformUtil.TryReformPawn(record, target.Value);
            }
            bool AtCorpse()
            {
                if(record.Pawn.Corpse == null) return false;
                var map = record.Pawn.Corpse.Map;
                if (map == null) return false;
                var cell = record.Pawn.Corpse.Position;
                return ReformUtil.TryReformPawn(record, map, cell);
            }
            bool AtDisposal()
            {
                var mapper = Rv2ReformersFind.ReformLocationUtilComponent;
                if (mapper == null) return false;
                var productContainer = mapper.GetContainerFor(record.Pawn);
                if(productContainer == null) return false;
                var map = productContainer.Map;
                if(map == null) return false;
                var cell = productContainer.Position;
                var result = ReformUtil.TryReformPawn(record, map, cell);
                return result;
            }
            bool Fallback() {
                return ReformUtil.TryReformPawn(record, scheduledRecord.FallbackTarget);
            }
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Collections.Look(ref scheduledReformations, "scheduledReformations", LookMode.Deep);
        }
        public void Add(ScheduledReformation scheduledReformation)
        {
            if (!GlobalReformTracker.HasRecord(scheduledReformation.Pawn))
            {
                RV2Log.Error("Attempted to add scheduled reform but the pawn didn't have a reform record", LogCategories.ScheduleReform);
                return;
            }
            RV2Log.Message($"Adding schduled reformation for {scheduledReformation.Pawn.Label} in {scheduledReformation.TicksTillReform} ticks", LogCategories.ScheduleReform);
            scheduledReformations.Add(scheduledReformation);
            GlobalReformTracker.RegisterReform(scheduledReformation.Pawn);
        }
        public void Add(Pawn pawn, int ticksTillReform, TargetInfo target)
        {
            Add(new ScheduledReformation(pawn, ticksTillReform, target, ScheduledReformLocationTargetType.AtDisposal));
        }
        public void Remove(Pawn pawn)
        {
            scheduledReformations.RemoveAll(item => item.Pawn == pawn);
        }
        public ScheduledReformation GetSchedule(Pawn pawn)
        {
            return scheduledReformations.FirstOrDefault(s=>s.Pawn == pawn);
        }
    }
}
