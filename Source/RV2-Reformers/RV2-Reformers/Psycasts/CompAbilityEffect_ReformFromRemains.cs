﻿using RimVore2;
using RimWorld;
using RimWorld.Planet;
using RV2_Reformers.Defs;
using RV2_Reformers.Reform;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Verse;
using static UnityEngine.GraphicsBuffer;

namespace RV2_Reformers.Psycasts
{
    public class CompAbilityEffect_ReformFromRemains : CompAbilityEffect
    {
        public new CompProperties_AbilityEffect Props => (CompProperties_AbilityEffect)props;
        List<ReformRecord> reformedRecords = new List<ReformRecord>();
        public override void Apply(LocalTargetInfo target, LocalTargetInfo dest)
        {
            base.Apply(target, dest);
            Log.Message($"Attempting to reform {target.Thing}");
            if (target.Thing == null) return;
            if (target.Thing is Corpse corpse) ProcessCorpse(target, corpse);
            else if (target.Thing is VoreProductContainer container) ProcessContainer(target, container);
            else
            {
                Log.Message($"Attempted to reform {target.Thing} but is not a valid target");
                return;
            }
            if (reformedRecords.Any()) PostReform(target);
            reformedRecords.Clear();
        }

        public override bool CanApplyOn(LocalTargetInfo target, LocalTargetInfo dest)
        {
            var canApply = target.Thing is Corpse || target.Thing is VoreProductContainer;
            Log.Message($"Can apply check {canApply}");
            return canApply;
        }

        private void PostReform(LocalTargetInfo target)
        {

        }

        private void ProcessContainer(LocalTargetInfo target, VoreProductContainer container)
        {
            ProcessCorpsesInContainer(target, container);
            ProcessLabelMatches(target, container);
        }
        private void ProcessLabelMatches(LocalTargetInfo target, VoreProductContainer container)
        {
            //Sadly vore containers don't store who the prey was only prodcuts
            //Not all vore options have the corpse inside the container
            //As a fallback I attempt to have it search for the name
            //in the label to see if we can find a matching record
            //this is far from ideal and I can see many issues with this
            //But best I can do with this psycast
            List<ReformRecord> matches = new List<ReformRecord>();
            foreach (var record in GlobalReformTracker.GetAllRecords())
            {
                var label = record.Pawn.NameFullColored;

                Log.Message($"Label search checking {label} in {container.Label}");
                if (container.Label.Contains(label))
                {
                    matches.Add(record);
                }
            }
            if (!matches.Any()) return;
            foreach (var record in matches)
            {
                ProcessRecord(target, record);
            }
        }
        private void ProcessCorpsesInContainer(LocalTargetInfo target, VoreProductContainer container)
        {
            var innerContainer = GetInnerContainer(container);
            if (innerContainer == null) return;
            var corpses = innerContainer.InnerListForReading.Where(i => i is Corpse).Cast<Corpse>().ToList();
            if(!corpses.Any()) return;
            foreach (var corpse in corpses)
            {
                Log.Message($"Processing corpse {corpse.Label}");
                ProcessCorpse(target, corpse);
            }
        }

        private void ProcessRecord(LocalTargetInfo target, ReformRecord record)
        {
            if (record == null) return;//can't reform prey who lack a record
            if (reformedRecords.Contains(record)) return;
            var quirkManager = record.Pawn?.QuirkManager(false);
            if(quirkManager != null && quirkManager.HasQuirk(Reformers_QuirksDefOfs.Cheat_UnReformable)) return;
            if (ReformUtil.TryReformPawn(record, this.parent.pawn.Map, target.Cell))
            {
                reformedRecords.Add(record);
            }
        }

        private void ProcessCorpse(LocalTargetInfo target, Corpse corpse)
        {
            var prey = corpse.InnerPawn;
            if(prey == null) return; //shouldnt happen but just in case
            var record = GlobalReformTracker.GetRecord(prey);
            ProcessRecord(target, record);
        }

        private ThingOwner<Thing> GetInnerContainer(VoreProductContainer container)
        {
            Type classType = typeof(VoreProductContainer);
            FieldInfo fieldInfo = classType.GetField("innerContainer", BindingFlags.NonPublic | BindingFlags.Instance);
            if(fieldInfo == null)
            {
                Log.Warning("Vore product container is missing the innerContainer field. RV2-Reformers may need to be updated.");
                return null;
            }
            return fieldInfo.GetValue(container) as ThingOwner<Thing>;
        }
    }
}
