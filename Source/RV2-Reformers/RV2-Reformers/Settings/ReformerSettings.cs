﻿using RimVore2;
using RV2_Reformers.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;
using static RV2_Reformers.Settings.SettingsPatches;

namespace RV2_Reformers.Settings
{
    public class ReformerSettings : ModSettings
    {
        public static SettingsContainer_ReformerGeneral General;
        public static SettingsContainer_ReformerPatches Patches;

        public ReformerSettings()
        {
            General = new SettingsContainer_ReformerGeneral();
            Patches = new SettingsContainer_ReformerPatches();
        }
        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Deep.Look(ref General, "General", new object[0]);
            Scribe_Deep.Look(ref Patches, "Patches", new object[0]);
        }
    }
}
