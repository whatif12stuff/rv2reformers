# Rv2Reformers
 Reformers sub mod for Rimvore2


Adds multiple methods to reform pawns after they have been digested.

The primary method is via a Reforming Machine, of which there are 2 avaliable/
The Arcotech Reformer is designed to be safe for those who want vore to be consequence free.
The Colony Reformer is requires power and requires reform caches to store pawn information.
Both existing reformers are only able to reform one pawn at a time.

Psycast Reformations
2 addtional psycasts abilities are added to allow reformation.
The normal reformation cast will attempt to reform a pawn from their remains. (Either vore product container or corpse)
The other psycast is a self-reofrmation. It requires the caster to designate a position to reform after a delay (about a day). This will have to be recast each time they reform.

Reformation Chip
This chip will attempt to reform the prey after a delay (about a day). They will normally reform at their remains or their pred's location.

This mod also includes patches relating to reforming pawns, these include:
	
1. Saving memories and relationship info prior to death and restoring them after reformation

2. Preventing pawns from getting upset due to death by digestion of loved ones/fellow colonists

3. Preventing garbage collection from deleting reform able pawns.

4. Preventing death notification for pawns who are automatically reformed

5. Restoration of work priorities



# Reformation Machine Setup
If you use a Arcotech reformer then all you have to do is build one of those and you should be good.

For colony reformer you will need to handle reform caches.
The colony reformer has a internal cache to stop a small amount of pawns. (As of writing it should be 5 total pawns)
This will store ALL pawns who are vored in the colony, this includes animals. At this time there isn't a way to designate who gets stored or not.

There are 2 reform Caches.
A basic reform cache is cheaper but only saves pawns who are digested on map, not useful for those in caravans.
A global reform cache is more expensive but will save pawns who are digested anywhere.
Both of these currently are setup to hold 5 total records

You can delete stored caches via clicking on the cache, selecting which records you want to remove via clicking the checkbox's and finally clicking the delete button.


# Known issues
Pawns genitals will sometimes disappear after being reformed. Keep Character Editor at hand just in case.


# Misc
The settings will automatically only store 50 total reform records. This is seperate then the cache records as the cache records merely point to reform records which is what holds the actual pawn info.
If a pawn lacks a reform record then they can't be reformed. Reform records unlike cache records do have a priority system, it will delete wild animals before it delete colonists for example. This means you shouldn't have to worry about losing your colonist because you declided to reform them for extended periods unless you've got a large amount of pawns.
You can increase or decrese to the total reform records stored in the settings.

If you accidently had your cache's full when a pawn is digested and lack any other way to reform them then you can open the reform record overview in the debug action menu.
In this you can click recache and it will attempt to recache the pawn, alternatively you can try and force a reform, or schedule a auto reformation.
Additionally in this menu you can delete reform records, do be aware there isn't a undo button.