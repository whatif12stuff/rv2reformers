﻿using HarmonyLib;
using RimVore2;
using RV2_Reformers.Constants;
using RV2_Reformers.Core;
using RV2_Reformers.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Verse;
using static RV2_Reformers.Settings.SettingsPatches;

namespace RV2_Reformers.Patches.RV2
{
    public class SettingsPatches
    {
        [HarmonyPatch(typeof(RV2Settings), "DefsLoaded")]
        public static class Patch_RV2Settings_DefsLoaded
        {

            [HarmonyPostfix]
            private static void Patch()
            {
                if (RV2ReformerMod.Settings == null) return;

                var fields = typeof(ReformerSettings).GetFields().Where(f => f.FieldType == typeof(SettingsContainer));
                foreach(var field in fields) {
                    var instance = field.GetValue(RV2ReformerMod.Settings) as SettingsContainer;
                    if (instance == null) continue;
                    instance.DefsLoaded();
                }
            }
        }
        [HarmonyPatch(typeof(RV2Settings), "Reset")]
        public static class Patch_RV2Settings_Reset
        {

            [HarmonyPostfix]
            private static void Patch()
            {
                if (RV2ReformerMod.Settings == null) return;

                var fields = typeof(ReformerSettings).GetFields().Where(f => f.FieldType == typeof(SettingsContainer));
                foreach (var field in fields)
                {
                    var instance = field.GetValue(RV2ReformerMod.Settings) as SettingsContainer;
                    if (instance == null) continue;
                    instance.Reset();
                }
            }
        }
        [HarmonyPatch(typeof(Window_Settings), "InitializeTabs")]
        public static class Patch_RV2Settings_InitializeTabs
        {

            [HarmonyPostfix]
            private static void Patch()
            {
                Window_Settings.AddTab(new SettingsTab_ReformerGeneral(StringKeys.SettingsGeneral.Translate(), null, null));
                Window_Settings.AddTab(new SettingsTab_ReformerPatches(StringKeys.SettingsPatches.Translate(), null, null));
            }
        }
        [HarmonyPatch(typeof(RV2Mod), "WriteSettings")]
        public static class Patch_RV2Mod_WriteSettings
        {
            [HarmonyPostfix]
            private static void AddDBHSettings()
            {
                RV2ReformerMod.Mod.WriteSettings();
            }
        }
    }
}
