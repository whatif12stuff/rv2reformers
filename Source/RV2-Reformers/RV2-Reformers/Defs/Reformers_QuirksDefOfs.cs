﻿using RimVore2;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RV2_Reformers.Defs
{
    [DefOf]
    public static class Reformers_QuirksDefOfs
    {
        public static QuirkDef Cheat_AlwaysReform;
        public static QuirkDef Cheat_UnReformable;
    }
}
