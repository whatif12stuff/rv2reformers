﻿using RimWorld;
using RV2_Reformers.ModCompatiblity.VE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RV2_Reformers.ModCompatiblity
{
    public static class Pawnmorpher
    {
        public static string[] PackageIds = { "tachyonite.pawnmorpherpublic", "tachyonite.pawnmorpher" };
        public const string MainPackageID = "tachyonite.pawnmorpherpublic";
    }
    [DefOf]
    public static class PawnmorpherDefs
    {
        [MayRequire(Pawnmorpher.MainPackageID)]
        public static ThoughtDef MyMergeMateDied;
        [MayRequire(Pawnmorpher.MainPackageID)]
        public static ThoughtDef KilledMyMergeMate;
        [MayRequire(Pawnmorpher.MainPackageID)]
        public static ThoughtDef MyExMergeDied;
        [MayRequire(Pawnmorpher.MainPackageID)]
        public static ThoughtDef KilledMyExMerge;

    }
}
