﻿using RimVore2;
using RimWorld.Planet;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace RV2_Reformers.ReformLocationUtil
{
    public class ReformLocationUtilComponent : WorldComponent
    {
        List<VoreProductContainerMapRecord> VoreProductContainerTrackers = new List<VoreProductContainerMapRecord>();
        private List<AssignedReformLocation> SetReformLocations = new List<AssignedReformLocation>();
        public ReformLocationUtilComponent(World world) : base(world)
        {
        }
        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Collections.Look(ref VoreProductContainerTrackers, "VoreProductMap", LookMode.Deep);
            Scribe_Collections.Look(ref SetReformLocations, "SetReformLocations", LookMode.Deep);
        }
        public TargetInfo? GetAssignedSpot(Pawn pawn)
        {
            return SetReformLocations.FirstOrDefault(a => a.pawn == pawn)?.targetInfo;
        }
        internal void RemoveAssignedSpot(Pawn prey)
        {
            SetReformLocations.RemoveAll(a=>a.pawn == prey);
        }
        public void SetReformLocation(Pawn pawn, Map map, IntVec3 position)
        {
            if(pawn == null) return;
            if(map == null) return;
            if(position == null) return;
            var Loc = SetReformLocations.FirstOrDefault(l=>l.pawn == pawn);
            if(Loc == null)
            {
                Loc = new AssignedReformLocation();
                Loc.pawn = pawn;
                SetReformLocations.Add(Loc);
            }
            Loc.targetInfo = new TargetInfo(position, map);
        }
        public VoreProductContainer GetContainerFor(Pawn pawn)
        {
            var tracker = VoreProductContainerTrackers.FirstOrDefault(record => record.pawns.Contains(pawn));
            if (tracker == null) return null;
            var container = tracker.container;
            if (container == null) this.VoreProductContainerTrackers.Remove(tracker);
            return container;
        }

        public void AddContainer(VoreProductContainer container, params Pawn[] pawns)
        {
            if (container == null) return;
            if (!pawns.Any()) return;
            if(VoreProductContainerTrackers.Any(t=>t.container == container))
            {
                return;
            }
            var record = new VoreProductContainerMapRecord
            {
                container = container,
                pawns = pawns.ToList()
            };
            this.VoreProductContainerTrackers.Add(record);
        }
        public void RemoveContainer(Pawn pawn)
        {
            var tracker = VoreProductContainerTrackers.FirstOrDefault(record => record.pawns.Contains(pawn));
            if (tracker == null) return;
            tracker.pawns.Remove(pawn);
            if (tracker.pawns.Any()) return;
            this.VoreProductContainerTrackers.Remove(tracker);
        }
        public void RemoveContainer(VoreProductContainer container) { 
            this.VoreProductContainerTrackers.RemoveAll(t=>t.container == container);
        }

    }
}
