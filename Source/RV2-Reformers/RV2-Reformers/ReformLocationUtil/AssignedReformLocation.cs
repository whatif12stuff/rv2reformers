﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace RV2_Reformers.ReformLocationUtil
{
    public class AssignedReformLocation : IExposable
    {
        public Pawn pawn;
        public TargetInfo targetInfo;

        public void ExposeData()
        {
            Scribe_References.Look(ref pawn, "pawn");
            Scribe_TargetInfo.Look(ref targetInfo, "targetInfo");
        }
    }
}
