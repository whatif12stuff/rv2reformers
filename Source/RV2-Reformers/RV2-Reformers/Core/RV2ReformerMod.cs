﻿using HarmonyLib;
using RimVore2;
using RV2_Reformers.Constants;
using RV2_Reformers.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Verse;

namespace RV2_Reformers.Core
{
    public class RV2ReformerMod : Mod
    {
        public static RV2ReformerMod Mod;
        public static ReformerSettings Settings { get; private set; }
        public RV2ReformerMod(ModContentPack content) : base(content) {
            Mod = this;
            Settings = GetSettings<ReformerSettings>();
        }

    }
}
