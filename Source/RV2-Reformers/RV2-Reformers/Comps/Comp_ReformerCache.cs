﻿using RimWorld;
using RV2_Reformers.Constants;
using RV2_Reformers.Reform;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace RV2_Reformers.Comps
{
    public class Comp_ReformerCache : ThingComp
    {
        #region Props
        private CompProperties_ReformerCache Props => (CompProperties_ReformerCache)props;
        public int MaxStoredRecords => Props.MaxCacheSize;
        public int CurrentCacheSize => CachedPrey.Count;
        public int FreeCache => Math.Max(0, MaxStoredRecords - CurrentCacheSize);
        public bool HasFreeCache => HasUnlimitedCache || FreeCache > 0;
        public bool HasGlobalReception => Props.IsGlobalReciver;
        public bool HasUnlimitedCache => MaxStoredRecords == -1;
        #endregion

        #region Power
        private CompPowerTrader _compPowerTrader;
        private bool hasLoadedPowerTraderComponent;
        protected CompPowerTrader PowerTrader => GetComp(ref _compPowerTrader, ref hasLoadedPowerTraderComponent);

        public bool HasPowerTrader => PowerTrader != null;
        public bool PowerOn => !HasPowerTrader || PowerTrader?.PowerOn == true;
        #endregion

        #region Combined Props
        public bool CanAcceptNewRecord => HasFreeCache && PowerOn;
        #endregion

        public IEnumerable<ReformRecord> CachedReformRecords => CachedPrey.Select(p=>GlobalReformTracker.GetRecord(p));

        private List<Pawn> CachedPrey = new List<Pawn>();
        public Comp_ReformerCache() : base()
        {
        }

        public override string CompInspectStringExtra()
        {
            if (this.HasUnlimitedCache) return StringKeys.CacheUnlimited.Translate();
            return StringKeys.CacheTotal.Translate(CurrentCacheSize, MaxStoredRecords);
        }

        public override void PostExposeData()
        {
            base.PostExposeData();
            Scribe_Collections.Look(ref CachedPrey, "preylist", true, LookMode.Reference);
        }

        #region Adding/Removing records
        public void Add(ReformRecord record)
        {
            Add(record.Pawn);
        }
        public void Add(Pawn pawn)
        {
            CachedPrey.Add(pawn);
        }
        public void Remove(ReformRecord record)
        {
            CachedPrey = CachedPrey.Where(p=>p != record.Pawn).ToList();
        }
        public void Remove(Pawn pawn)
        {
            CachedPrey.Remove(pawn);
        }
        #endregion

        internal bool HasRecord(ReformRecord record)
        {
            return CachedPrey.Any(p => p == record.Pawn);
        }

        public bool CanHearMap(Map map)
        {
            if (this.HasGlobalReception) return true;
            return map == this.parent.Map;
        }

        #region Class Util
        private T GetComp<T>(ref T compField, ref bool loadedFlag) where T : ThingComp
        {
            if (!loadedFlag)
            {
                compField = this.parent.TryGetComp<T>();
                loadedFlag = true;
            }
            return compField;
        }
        #endregion
    }
}
