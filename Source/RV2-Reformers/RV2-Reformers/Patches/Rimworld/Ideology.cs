﻿using HarmonyLib;
using RimVore2;
using RimWorld;
using RV2_Reformers.Debug;
using RV2_Reformers.Reform;
using RV2_Reformers.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace RV2_Reformers.Patches
{
    [HarmonyPatch(typeof(Ideo), nameof(Ideo.Notify_MemberDied))]
    internal class Ideology
    {
        [HarmonyPrefix]
        public static bool Patch(Pawn member)
        {
            try
            {
                if (!GetSetting.PatchIdeoMemberDeath) return true;
                return !GlobalReformTracker.IsReforming(member);
            } catch(Exception e)
            {
                RV2Log.Error("Caught Exception in Ideo Patch. " + e, LogCategories.ReformPatches);
            }
            return true;
        }
    }
}
