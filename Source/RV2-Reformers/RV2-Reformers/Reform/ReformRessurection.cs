﻿using RimVore2;
using RimWorld;
using RV2_Reformers.Debug;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse.AI.Group;
using Verse;
using RV2_Reformers.Constants;
using HarmonyLib;
using static RimWorld.GoodwillSituationManager;
using RV2_Reformers.Utilities;

namespace RV2_Reformers.Reform
{
    public static class ReformRessurection
    {
        public static bool TryRessurect(ReformRecord record, out string failReason)
        {
            var pawn = record.Pawn;
            if(pawn == null)
            {
                failReason = StringKeys.NullPawn;
                RV2Log.Error("Reform record null pawn requested ressurection.", LogCategories.Ressurect);
                return false;
            }

            try
            {
#if v1_4 || v1_3
                ResurrectionUtility.Resurrect(pawn);
                failReason = "";
                return true;
#else
                ResurrectionParams ressurectionParams = new ResurrectionParams()
                {
                    invisibleStun = false,
                    dontSpawn = true,
                };
                var result = ResurrectionUtility.TryResurrect(pawn, ressurectionParams);
                failReason = result ? "" : "Ressurection Utility Failed";
                return result;
#endif
            }catch(Exception ex)
            {
                failReason = StringKeys.RessurectionException;
                RV2Log.Error($"Exception trying to do standard pawn ressurection! {ex}", LogCategories.Ressurect);
                return false;
            }
        }
        public static void ApplyRecordCache(ReformRecord record)
        {
            RestoreWorkSettings(record);
            RestoreMemories(record);
            RestoreThoughts(record);
        }
        public static bool TeleportToPosition(ReformRecord record, Map map, IntVec3 Position)
        {
            return GenPlace.TryPlaceThing(record.Pawn, Position, map, ThingPlaceMode.Direct);
        }
        private static void RestoreWorkSettings(ReformRecord record)
        {
            if (record == null) return;
            if (record.workSettings == null) return;
            var current = record.Pawn.workSettings;
            if (current == null)
            {
                record.Pawn.workSettings = new Pawn_WorkSettings();
                current = record.Pawn.workSettings;
            }
            var typeInternalPawnReference = typeof(Pawn_WorkSettings).GetField("pawn", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
            var internalPawn = typeInternalPawnReference.GetValue(record.workSettings);
            if(internalPawn == null)
            {
                typeInternalPawnReference.SetValue(record.workSettings, record.Pawn);
            }

            current.EnableAndInitializeIfNotAlreadyInitialized();
            foreach(var type in DefDatabase<WorkTypeDef>.AllDefsListForReading)
            {
                var oldValue = record.workSettings.GetPriority(type);
                current.SetPriority(type, oldValue);
            }
        }
        private static void RestoreMemories(ReformRecord record)
        {
            var Thoughts = PawnAccess.GetMemories(record.Pawn);
            if(Thoughts == null)
            {
                RV2Log.Warning($"Memory list was null when reforming pawn {record.Pawn.Label}.", LogCategories.Ressurect);
                return;
            }
            Thoughts.AddRange(record.CachedMemories);
            Thoughts.ForEach(t => t.pawn = record.Pawn);//Can point to wrong thing after load so reset pawn info
        }
        private static void RestoreThoughts(ReformRecord record)
        {
            var Thoughts = PawnAccess.GetSituationalThoughts(record.Pawn);
            if (Thoughts == null)
            {
                RV2Log.Warning($"Situational Though list was null when reforming pawn {record.Pawn.Label}.", LogCategories.Ressurect);
                return;
            }
            Thoughts.AddRange(record.CachedSituationals);
            Thoughts.ForEach(t => t.pawn = record.Pawn);//Can point to wrong thing after load so reset pawn info
        }
    }
}
