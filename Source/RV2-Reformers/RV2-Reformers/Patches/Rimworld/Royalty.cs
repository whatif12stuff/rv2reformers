﻿using HarmonyLib;
using RimVore2;
using RimWorld;
using RV2_Reformers.Debug;
using RV2_Reformers.Reform;
using RV2_Reformers.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace RV2_Reformers.Patches.Rimworld
{
    [HarmonyPatch(typeof(Pawn_RoyaltyTracker), nameof(Pawn_RoyaltyTracker.Notify_PawnKilled))]
    internal class DeathPatch
    {
        [HarmonyPrefix]
        public static bool Patch(Pawn_RoyaltyTracker __instance)
        {
            var pawn = __instance.pawn;
            try
            {
                return !GlobalReformTracker.IsReforming(pawn); ;
            }
            catch (Exception e)
            {
                RV2Log.Error("Caught Exception in Royalty Patch. " + e, LogCategories.ReformPatches);
            }
            return true;
        }
    }
}