﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RV2_Reformers.ModCompatiblity.Immortals
{
    public enum ImmortalsReformationOption
    {
        None,
        SelfReform,
    }
    public enum ImmortalsTransferOption
    {
        None,
        AlwaysTransfer,
        TransferToImmortals,
        TransferToKnownImmortals,
    }
}
