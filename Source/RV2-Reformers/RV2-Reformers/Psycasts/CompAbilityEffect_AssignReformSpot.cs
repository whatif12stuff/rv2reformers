﻿using RimWorld;
using RV2_Reformers.Reform;
using RV2_Reformers.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace RV2_Reformers.Psycasts
{
    public class CompAbilityEffect_AssignReformSpot : CompAbilityEffect
    {
        public new CompProperties_AbilityEffect Props => (CompProperties_AbilityEffect)props;

        public override void Apply(LocalTargetInfo target, LocalTargetInfo dest)
        {
            base.Apply(target, dest);
            Rv2ReformersFind.ReformLocationUtilComponent.SetReformLocation(this.parent.pawn, this.parent.pawn.Map, target.Cell);
        }
    }
}
