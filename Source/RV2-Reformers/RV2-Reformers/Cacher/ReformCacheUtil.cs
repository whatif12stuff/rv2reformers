﻿using HarmonyLib;
using RimVore2;
using RV2_Reformers.Comps;
using RV2_Reformers.Defs;
using RV2_Reformers.Reform;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace RV2_Reformers.Cacher
{
    public static class ReformCacheUtil
    {
        public static bool AddForNewRecord(ReformRecord record)
        {
            if(record == null) throw new ArgumentNullException(nameof(record));
            if(record.Pawn == null) throw new NullReferenceException(nameof(record.Pawn));
            var quirkManager = record.Pawn.QuirkManager(false);
            if (quirkManager != null && quirkManager.HasQuirk(Reformers_QuirksDefOfs.Cheat_UnReformable)) return false;
            var originMap = record.Pawn.GetVoreRecord()?.TopPredator?.Map;
            if (AttemptToAddToMapCaches(record, originMap)) return true;
            return AttemptToAddToGlobalCaches(record);
        }
        public static void RemoveRecord(ReformRecord record)
        {
            if (record == null) throw new ArgumentNullException(nameof(record));
            if (record.Pawn == null) throw new NullReferenceException(nameof(record.Pawn));
            foreach(var map in Find.Maps)
            {
                foreach(var cache in GetMapCaches(map))
                {
                    cache.Remove(record);
                    ReformUtil.OnDeleted(record);
                }
            }
        }

        private static bool AttemptToAddToGlobalCaches(ReformRecord record)
        {
            var caches = GetGlobalCaches().Where(c=>c.CanAcceptNewRecord);
            if (caches.EnumerableNullOrEmpty()) return false;
            caches.First().Add(record);
            return true;
        }

        private static bool AttemptToAddToMapCaches(ReformRecord record, Map map)
        {
            if(map == null) return false;
            var caches = GetMapCaches(map).Where(c=>c.CanAcceptNewRecord);
            if (caches.EnumerableNullOrEmpty()) return false;
            caches.First().Add(record);
            return true;
        }


        #region Find caches
        public static IEnumerable<Comp_ReformerCache> GetMapCaches(Map map)
        {
            //I could have the cache's be stored upon create and removed upon being destroyed 
            foreach (var thing in map.listerThings.AllThings)
            {
                var comp = thing.TryGetComp<Comp_ReformerCache>();
                if(comp == null) continue;
                yield return comp;
            }
        }
        public static IEnumerable<Comp_ReformerCache> GetGlobalCaches()
        {
            List<Comp_ReformerCache> list = new List<Comp_ReformerCache>();
            foreach (var map in Find.Maps)
            {
                list.AddRange(GetMapCaches(map).Where(c => c.HasGlobalReception));
            }
            return list;
        }

        public static Comp_ReformerCache GetCacheForRecord(ReformRecord record)
        {
            if(record == null) return null;
            foreach(var map in Find.Maps)
            {
                var caches = GetMapCaches(map);
                var cache = caches.FirstOrDefault(c => c.HasRecord(record));
                if(cache == null) continue;
                return cache;
            }
            return null;
        }

        internal static bool IsCached(Pawn prey)
        {
            return GetCacheForRecord(GlobalReformTracker.GetRecord(prey)) != null;
        }
        #endregion
    }
}
