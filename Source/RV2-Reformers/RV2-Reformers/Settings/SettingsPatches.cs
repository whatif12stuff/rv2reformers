﻿using RimVore2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Verse;

namespace RV2_Reformers.Settings
{
    public class SettingsPatches
    {
        public class SettingsTab_ReformerPatches : SettingsTabFillable
        {
            public SettingsTab_ReformerPatches(string label, Action clickedAction, bool selected) : base(label, clickedAction, selected)
            {
            }
            public SettingsTab_ReformerPatches(string label, Action clickedAction, Func<bool> selected) : base(label, clickedAction, selected)
            {
            }

            public override SettingsContainerFillable FillableContainer => (SettingsContainerFillable)ReformerSettings.Patches;
        }


        public class SettingsContainer_ReformerPatches : SettingsContainerFillable
        {
            public bool PatchIdeoMemberDeath => _PatchIdeoMemberDeath.value;
            public bool PatchPlayerDeathNotification => _PatchPlayerDeathNotification.value;
            public bool PatchRelationTracker => _PatchRelationTracker.value;
            public bool PatchThoughtsUtility => _PatchThoughtsUtility.value;


            public bool PatchVanillaExpandedPawnKilled => _PatchVanillaExpandedPawnKilled.value;

            private BoolSmartSetting _PatchIdeoMemberDeath;
            private BoolSmartSetting _PatchPlayerDeathNotification;
            private BoolSmartSetting _PatchRelationTracker;
            private BoolSmartSetting _PatchThoughtsUtility;

            private BoolSmartSetting _PatchVanillaExpandedPawnKilled;

            private bool heightStale = true;
            private float height = 0f;
            private Vector2 scrollPosition;


            public override void FillRect(Rect inRect)
            {
                Rect outerRect = inRect;
                UIUtility.MakeAndBeginScrollView(outerRect, height, ref scrollPosition, out Listing_Standard list);

                try
                {
                    if (list.ButtonText("RV2Reformer-Settings_Reset".Translate()))
                        Reset();

                    _PatchIdeoMemberDeath.DoSetting(list);
                    _PatchPlayerDeathNotification.DoSetting(list);
                    _PatchRelationTracker.DoSetting(list);
                    _PatchThoughtsUtility.DoSetting(list);
                }
                catch (Exception e)
                {
                    Widgets.TextArea(outerRect, e.Message, true);
                    EnsureSmartSettingDefinition();
                }

                list.EndScrollView(ref height, ref heightStale);
            }

            public override void Reset()
            {
                _PatchIdeoMemberDeath = null;
                _PatchPlayerDeathNotification = null;
                _PatchRelationTracker = null;
                _PatchThoughtsUtility = null;

                EnsureSmartSettingDefinition();
            }
            public override void ExposeData()
            {
                base.ExposeData();
                if (Scribe.mode == LoadSaveMode.Saving || Scribe.mode == LoadSaveMode.LoadingVars)
                {
                    EnsureSmartSettingDefinition();
                }
                Scribe_Deep.Look(ref _PatchIdeoMemberDeath, "PatchIdeoMemberDeath", new object[0]);
                Scribe_Deep.Look(ref _PatchPlayerDeathNotification, "PatchPlayerDeathNotification", new object[0]);
                Scribe_Deep.Look(ref _PatchRelationTracker, "PatchRelationTracker", new object[0]);
                Scribe_Deep.Look(ref _PatchThoughtsUtility, "PatchThoughtsUtility", new object[0]);
                Scribe_Deep.Look(ref _PatchVanillaExpandedPawnKilled, "PatchVanillaExpandedPawnKilled", new object[0]);

            }
            public override void EnsureSmartSettingDefinition()
            {
                if (_PatchIdeoMemberDeath?.IsInvalid() != false)
                    _PatchIdeoMemberDeath = new BoolSmartSetting("Settings_PatchIdeoMemberDeath", true, true, "SettingsTooltip_PatchIdeoMemberDeath");
                if (_PatchPlayerDeathNotification?.IsInvalid() != false)
                    _PatchPlayerDeathNotification = new BoolSmartSetting("Settings_PatchPlayerDeathNotification", true, true, "SettingsTooltip_PatchPlayerDeathNotification");
                if (_PatchRelationTracker?.IsInvalid() != false)
                    _PatchRelationTracker = new BoolSmartSetting("Settings_PatchRelationTracker", true, true, "SettingsTooltip_PatchRelationTracker");
                if (_PatchThoughtsUtility?.IsInvalid() != false)
                    _PatchThoughtsUtility = new BoolSmartSetting("Settings_PatchThoughtsUtility", true, true, "SettingsTooltip_PatchThoughtsUtility");
                if (_PatchVanillaExpandedPawnKilled?.IsInvalid() != false)
                    _PatchVanillaExpandedPawnKilled = new BoolSmartSetting("Settings_PatchVanillaExpandedPawnKilled", true, true, "SettingsTooltip_PatchVanillaExpandedPawnKilled");
            }
        }
    }
}
