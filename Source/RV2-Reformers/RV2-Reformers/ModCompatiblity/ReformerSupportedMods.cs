﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RV2_Reformers.ModCompatiblity
{
    public static class ReformerSupportedMods
    {
        public const string VanillaSocialInteractionsExpanded = "VanillaExpanded.VanillaSocialInteractionsExpanded";
        public const string ImmortalsMod = "fridgebaron.immortals";
    }
}
