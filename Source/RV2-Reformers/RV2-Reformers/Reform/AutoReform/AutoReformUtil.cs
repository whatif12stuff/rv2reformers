﻿using RimVore2;
using RimWorld;
using RV2_Reformers.Comps;
using RV2_Reformers.Constants;
using RV2_Reformers.Debug;
using RV2_Reformers.Defs;
using RV2_Reformers.ReformLocationUtil;
using RV2_Reformers.Reform;
using RV2_Reformers.Reform.AutoReform;
using RV2_Reformers.Reform.ScheduledReforms;
using RV2_Reformers.Settings;
using RV2_Reformers.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;
using RV2_Reformers.Cacher;
using RV2_Reformers.ModCompatiblity.Immortals;

namespace RV2_Reformers.Reform
{
    public static class AutoReformUtil
    {
        public static ReformRequestResult RequestReform(ReformRecord reformRecord)
        {
            var result = CheckRequestValid(reformRecord);
            Log.Message($"Checking if valid {result.RequestSuccessful}? {result.Reasoning}");
            if(!result.RequestSuccessful) return result;

            result = RequestViaSelfReforming(reformRecord);
            if(result.RequestSuccessful) return result;
            return RequestViaReformingMachine(reformRecord);
        }

        private static ReformRequestResult RequestViaSelfReforming(ReformRecord reformRecord)
        {
            var pawn = reformRecord.Pawn;
            var quirkManager = reformRecord.Pawn?.QuirkManager(false);
            if(quirkManager != null
               && quirkManager.HasQuirk(Reformers_QuirksDefOfs.Cheat_AlwaysReform)
            )
            {
                return DoTimedReform(reformRecord);
            }
            if (
                reformRecord.HasReformChip || 
                pawn.health?.hediffSet != null
                && pawn.health.hediffSet.HasHediff(Reformers_HediffDefOfs.ReformChipHediff)
            ) {
                return DoTimedReform(reformRecord);
            }

            if (ModsCheckerUtil.IsImmortalsActive)
            {
                if (ImmortalsUtil.ShouldAutoReform(reformRecord))
                {
                    return DoTimedReform(reformRecord);
                }
            }
            
            if (
                ModsConfig.RoyaltyActive && 
                pawn.HasPsylink
                && pawn.abilities.GetAbility(Reformer_PsycastsDefOfs.Rv2Reformers_ReformSelf) != null
                && Rv2ReformersFind.ReformLocationUtilComponent.GetAssignedSpot(pawn) != null
            ) {
                var result = DoTimedReform(reformRecord);
                if (result.RequestSuccessful)
                {
                    Rv2ReformersFind.ReformLocationUtilComponent.RemoveAssignedSpot(pawn);
                    return result;
                }
            }

            return new ReformRequestResult()
            {
                RequestSuccessful = false,
                Reasoning = StringKeys.ReformRequestFail_NoSelfReformationAvaliable
            };
        }

        public static ReformRequestResult RequestViaReformingMachine(ReformRecord reformRecord)
        {
            var recordCache = ReformCacheUtil.GetCacheForRecord(reformRecord);
            if (recordCache == null) return new ReformRequestResult() {
                Reasoning = StringKeys.RequestFailMissingCachedRecord,
                RequestSuccessful = false,
                SilentFail = false
            };

            var choosenReformer = FindBestReformer(reformRecord, recordCache.parent.Map);
            if (choosenReformer == null)
            {
                return new ReformRequestResult()
                {
                    Reasoning = StringKeys.RequestFailMissingReformer,
                    RequestSuccessful = false,
                    SilentFail = false,
                };
            }

            DoAutoReform(choosenReformer, reformRecord);

            return new ReformRequestResult()
            {
                ReformingThing = choosenReformer.parent,
                RequestSuccessful = true,
                ReformType = ReformRequestType.ByThing
            };
        }

        public static ReformRequestResult DoTimedReform( ReformRecord record)
        {
            var topPred = GlobalVoreTrackerUtility.GetVoreRecord(record.Pawn).TopPredator;
            TargetInfo target = new TargetInfo(topPred.Position, topPred.Map, true);
            int ticks = Rand.Range(GetSetting.MinSelfReformTime, GetSetting.MaxSelfReformTime);
            var scheduled = new ScheduledReformation()
            {
                Pawn = record.Pawn,
                FallbackTarget = target,
                TicksTillReform = ticks,
                RefomationLocation = ScheduledReformLocationTargetType.AtDisposal
            };
            Rv2ReformersFind.ScheduleReformationManager.Add(scheduled);

            return new ReformRequestResult()
            {
                RequestSuccessful = true,
                ReformType = ReformRequestType.AutoReform
            };
        }
        private static void DoAutoReform(Comp_Reformer reformer, ReformRecord record)
        {
            reformer.AddPawn(record.Pawn);
        }


        //Todo priority system
        public static Comp_Reformer FindBestReformer(ReformRecord record, Map map)
        {            
            if (map != null)
            {
                var mapReformers = ReformerFinder.PlayerOwnedReformersOnMap(map).Where(b => b.IsAutomaticOn && b.AbleToAddToReformQueue);

                return PickBestReformer(mapReformers);
            }

            var globalReformers = ReformerFinder.PlayerOwnedGlobalReformers().Where(b => b.IsAutomaticOn && b.AbleToAddToReformQueue);
            return PickBestReformer(globalReformers);
        }

        private static Comp_Reformer PickBestReformer(IEnumerable<Comp_Reformer> reformers)
        {
            return reformers.OrderByDescending((Comp_Reformer c) => c.QueueCount).FirstOrDefault();
        }

        
        
        private static ReformRequestResult CheckRequestValid(ReformRecord Record)
        {
            if (GlobalReformTracker.IsReforming(Record.Pawn))
            {
                return new ReformRequestResult()
                {
                    Reasoning = StringKeys.AlreadyReforming,
                    RequestSuccessful = false,
                    SilentFail = true
                };
            }

            if (Record.Pawn.Faction != Find.FactionManager.OfPlayer) {
                return new ReformRequestResult()
                {
                    Reasoning = StringKeys.RequestFailNonPlayerFaction,
                    RequestSuccessful = false,
                    SilentFail = true
                };
            }

            return new ReformRequestResult()
            {
                RequestSuccessful = true
            };
        }
    }
}
