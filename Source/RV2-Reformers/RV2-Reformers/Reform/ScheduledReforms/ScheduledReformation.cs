﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Verse;

namespace RV2_Reformers.Reform.ScheduledReforms
{
    public class ScheduledReformation : IExposable
    {
        public Pawn Pawn;
        public int TicksTillReform;
        public TargetInfo FallbackTarget;
        public ScheduledReformLocationTargetType RefomationLocation;
        public bool IsInvalid => this.Pawn?.Dead != true;
        public bool ShouldReform => this.TicksTillReform <= 0 && !IsInvalid;

        public ScheduledReformation() { }
        public ScheduledReformation(Pawn pawn, int ticksTillReform, TargetInfo FallbackTarget, ScheduledReformLocationTargetType RefomationLocation)
        {
            this.Pawn = pawn;
            this.TicksTillReform = ticksTillReform;
            this.FallbackTarget = FallbackTarget;
            this.RefomationLocation = RefomationLocation;
        }
        public void ExposeData()
        {
            Scribe_References.Look(ref Pawn, "pawn", true);
            Scribe_Values.Look(ref TicksTillReform, "TicksTillReform");
            Scribe_TargetInfo.Look(ref FallbackTarget, "ReformTarget");
        }
    }
    public enum ScheduledReformLocationTargetType
    {
        DefaultTarget,
        AtDisposal,
    }
}
