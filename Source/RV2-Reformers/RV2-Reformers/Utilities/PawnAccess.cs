﻿using HarmonyLib;
using RimVore2;
using RimWorld;
using RV2_Reformers.Debug;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace RV2_Reformers.Utilities
{
    public static class PawnAccess
    {
        public static ThoughtHandler GetThoughtHandler(Pawn pawn)
        {
            return pawn.needs?.mood?.thoughts;
        }
        public static MemoryThoughtHandler GetMemoryThoughtHandler(Pawn pawn)
        {
            return GetThoughtHandler(pawn)?.memories;
        }
        public static SituationalThoughtHandler GetSituationalThoughtHandler(Pawn pawn)
        {
            return GetThoughtHandler(pawn)?.situational;
        }
        public static List<Thought_Memory> GetMemories(Pawn pawn)
        {
            if (pawn == null) return null;
            var handler = GetMemoryThoughtHandler(pawn);
            if(handler == null) return null;
            return AccessTools.Field(typeof(MemoryThoughtHandler), "memories").GetValue(handler) as List<Thought_Memory>;
        }
        public static List<Thought_Situational> GetSituationalThoughts(Pawn pawn)
        {
            if (pawn == null) return null;
            var handler = GetSituationalThoughtHandler(pawn);
            if(handler == null) return null;
            return AccessTools.Field(typeof(SituationalThoughtHandler), "cachedThoughts").GetValue(handler) as List<Thought_Situational>;
        }
    }
}
