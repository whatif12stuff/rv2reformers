﻿using HarmonyLib;
using RimVore2;
using RimWorld.Planet;
using RV2_Reformers.Debug;
using RV2_Reformers.Reform;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace RV2_Reformers.Patches.Rimworld
{
    [HarmonyPatch(typeof(WorldPawnGC), "GetCriticalPawnReason")]
    public class WorldPawnGCPatch
    {
        [HarmonyPostfix]
        private static void Patch(Pawn pawn, ref string __result)
        {
            //No need to check if they are already critical
            if (__result != null) return;

            try
            {
                if (GlobalReformTracker.HasRecord(pawn))
                {
                    RV2Log.Message($"Prevented pawn {pawn.LabelShort} from being GC'd due to existing reform record", LogCategories.ReformPatches);
                    __result = "ReformRecordExists";
                }
            }
            catch(Exception e)
            {
                RV2Log.Error("Caught exception in WorldPawnGC. " + e, LogCategories.ReformPatches);
            }
        }
    }
}
