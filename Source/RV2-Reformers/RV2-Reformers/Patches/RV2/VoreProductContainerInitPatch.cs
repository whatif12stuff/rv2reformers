﻿using HarmonyLib;
using RimVore2;
using RV2_Reformers.ReformLocationUtil;
using RV2_Reformers.Reform.AutoReform;
using RV2_Reformers.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace RV2_Reformers.Patches.RV2
{
    [HarmonyPatch(typeof(VoreProductContainer), nameof(VoreProductContainer.Initialize))]
    public class ProductContainerInitPatch
    {
        [HarmonyPostfix]
        public static void Postfix(ref VoreProductContainer __instance, Pawn deadPrey)
        {
            Rv2ReformersFind.ReformLocationUtilComponent.AddContainer(__instance, deadPrey);
        }
    }
}
