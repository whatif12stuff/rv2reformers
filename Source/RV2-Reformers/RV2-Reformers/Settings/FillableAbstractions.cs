﻿using RimVore2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace RV2_Reformers.Settings
{
    public abstract class SettingsContainerFillable : SettingsContainer
    {
        public abstract void FillRect(Rect inRect);
    }
    public abstract class SettingsTabFillable : SettingsTab
    {
        public abstract SettingsContainerFillable FillableContainer { get; }
        public override SettingsContainer AssociatedContainer => FillableContainer;
        public SettingsTabFillable(string label, Action clickedAction, bool selected) : base(label, clickedAction, selected)
        {
        }
        public SettingsTabFillable(string label, Action clickedAction, Func<bool> selected) : base(label, clickedAction, selected) { }
        public override void FillRect(Rect inRect)
        {
            FillableContainer?.FillRect(inRect);
        }
    }
}
