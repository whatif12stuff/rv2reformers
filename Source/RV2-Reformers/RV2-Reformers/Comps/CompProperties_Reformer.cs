﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace RV2_Reformers.Comps
{
    public class CompProperties_Reformer : CompProperties
    {
        public float ReformTicksMultiplier = 1;
        public bool Automatic = false;
        public bool Global = false;

        public CompProperties_Reformer()
        {
            compClass = typeof(Comp_Reformer);
        }
    }
}
