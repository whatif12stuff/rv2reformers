﻿#if !(v1_4 || v1_3)
using LudeonTK;
#endif
using RimVore2;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace RV2_Reformers.Dev
{
    
    public static class FixQuirkLayerNullInternalPawnRef
    {
        [DebugAction("Rv2Reformers", "Fix Quirk Layer Internal Pawn References", actionType = DebugActionType.Action, allowedGameStates = AllowedGameStates.PlayingOnMap)]
        public static void DebugAction()
        {
            FieldInfo pawnField = typeof(QuirkLayer_Persistent).GetField("pawn", BindingFlags.Instance | BindingFlags.NonPublic);
            if (pawnField == null)
            {
                Log.Error($"Pawn field was null");
                return;
            }
            FieldInfo persistentQuirkLayerField = typeof(QuirkManager).GetField("persistentQuirkLayer", BindingFlags.Instance | BindingFlags.NonPublic);
            if (pawnField == null)
            {
                Log.Error($"persistentQuirkLayer field was null");
                return;
            }

            foreach (var pawn in PawnsFinder.All_AliveOrDead)
            {
                var pawndata = pawn.PawnData(false);

                if (pawndata == null) continue;
                var manager = pawndata.QuirkManager(false);
                if (manager == null) continue;

                var persistentQuirkLayer = persistentQuirkLayerField.GetValue(manager) as QuirkLayer_Persistent;
                if (persistentQuirkLayer == null) continue;
                var value = pawnField.GetValue(persistentQuirkLayer);

                if (value != null) continue;
                Log.Message($"{pawn.Label} had a broken persistent quirk layer internal pawn reference. Fixing.");
                pawnField.SetValue(persistentQuirkLayer, pawn);
            }
        }
    }
}
