﻿using HarmonyLib;
using RimWorld;
using RV2_Reformers.Settings;
using RV2_Reformers.Reform;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;
using RimVore2;
using RV2_Reformers.Debug;

namespace RV2_Reformers.Patches
{
    [HarmonyPatch(typeof(Pawn_RelationsTracker), "Notify_PawnKilled")]
    internal class RelationTracker
    {
        [HarmonyPrefix]
        public static bool Patch(Pawn_RelationsTracker __instance, DamageInfo? dinfo, Map mapBeforeDeath, Pawn ___pawn)
        {
            try
            {
                if (!GetSetting.PatchRelationTracker) return true;
                return !GlobalReformTracker.IsReforming(___pawn);
            }catch (Exception e)
            {
                RV2Log.Error("Exception caught in Pawn_RelationsTracker " + e, LogCategories.ReformPatches);
            }
            return true;
        }
    }
}
