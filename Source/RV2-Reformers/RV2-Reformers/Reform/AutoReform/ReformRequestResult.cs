﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace RV2_Reformers.Reform
{
    public class ReformRequestResult
    {
        public ReformRequestType ReformType = ReformRequestType.NotApplicable;

        public bool RequestSuccessful;
        public string Reasoning;
        public Thing ReformingThing;
        public bool SilentFail;
    }
    public enum ReformRequestType
    {
        NotApplicable,
        ByThing,
        AutoReform
    }
}
