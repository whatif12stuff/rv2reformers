﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RV2_Reformers.Defs
{
    [DefOf]
    public static class Reformer_IdeoDefOfs
    {
        public static PreceptDef Rv2Reformers_VoreDeath_Normal;
        public static PreceptDef Rv2Reformers_VoreDeath_DontCare;
        public static PreceptDef Rv2Reformers_VoreDeath_ReformerExists;
        public static PreceptDef Rv2Reformers_VoreDeath_Reforms;
        public static PreceptDef Rv2Reformers_VoreDeath_Cached;
    }
}
