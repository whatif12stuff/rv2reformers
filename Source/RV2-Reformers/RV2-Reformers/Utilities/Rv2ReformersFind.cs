﻿using RV2_Reformers.ReformLocationUtil;
using RV2_Reformers.Reform.AutoReform;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace RV2_Reformers.Utilities
{
    public static class Rv2ReformersFind
    {
        public static ReformLocationUtilComponent ReformLocationUtilComponent => Find.World.GetComponent<ReformLocationUtilComponent>();
        public static ScheduleReformationManager ScheduleReformationManager => Find.World.GetComponent<ScheduleReformationManager>();
    }
}
