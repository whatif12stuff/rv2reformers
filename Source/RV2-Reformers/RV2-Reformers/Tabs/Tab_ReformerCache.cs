﻿using RimVore2.Tabs;
using RimWorld;
using RV2_Reformers.Cacher;
using RV2_Reformers.Comps;
using RV2_Reformers.Constants;
using RV2_Reformers.Reform;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Verse;

namespace RV2_Reformers.Tabs
{
    public class Tab_ReformerCache : ITab
    {
        private const int ContentMarginX = 5;
        private const int ContentMarginY = 25;
        private const int ScrollbarWidth = 25;
        private const int RecordRowHeight = 25;
        private const float CheckboxSize = 20;
        private const float DeleteButtonHeight = 30;
        private const float DeleteButtonWidth = 100;
        private const float CancelButtonWidth = 100;
        private const float SpaceAfterDeleteButton = 10;
        private const float BottomPadding = 10;
        private static readonly Vector2 MaxSize = new Vector2(500f, 450f);
        private static readonly Vector2 MinSize = new Vector2(500f, 200f);

        private Vector2 scrollPosition = Vector2.zero;
        private float ScrollHeight = 0;
        private HashSet<ReformRecord> selectedRecords = new HashSet<ReformRecord>();

        public Comp_ReformerCache Source
        {
            get
            {
                return this.SelThing?.TryGetComp<Comp_ReformerCache>();
            }
        }

        public Tab_ReformerCache()
        {
            size = new Vector2(400f, 0f);
            this.labelKey = StringKeys.Tab_CacheView;
        }

        public override void OnOpen()
        {
            base.OnOpen();
        }

        protected override void FillTab()
        {
            VoreTabHelper.ResetGUI();
            Rect contentRect = new Rect(ContentMarginX, ContentMarginY, size.x - 2 * ContentMarginX, size.y - 2 * ContentMarginY + BottomPadding);
            Rect innerRect = new Rect(contentRect);
            innerRect.height = ScrollHeight;
            innerRect.width -= ScrollbarWidth;

            Widgets.BeginScrollView(contentRect, ref scrollPosition, innerRect);
            innerRect.height = 0;
            try
            {
                innerRect.height = CalculateInitialHeightForRecords();

                DrawContent(ref innerRect);

                ScrollHeight = innerRect.height;

            }
            catch (Exception e)
            {
                Widgets.Label(contentRect, $"Exception rendering UI: {e}");
            }
            VoreTabHelper.ResetGUI();
            Widgets.EndScrollView();
            size.y = Math.Max(Math.Min(MaxSize.y, ScrollHeight + ContentMarginY * 2), MinSize.y);
        }

        private void DrawContent(ref Rect rect)
        {
            float deleteButtonHeight = DrawDeleteButton(ref rect);
            rect.height += deleteButtonHeight + SpaceAfterDeleteButton;
            DrawRecords(ref rect);
        }

        private float DrawDeleteButton(ref Rect rect)
        {
            Rect buttonRect = new Rect(rect.x, rect.y, DeleteButtonWidth, DeleteButtonHeight);

            if (Widgets.ButtonText(buttonRect, StringKeys.Delete.Translate()))
            {
                DeleteSelected();
            }
            return buttonRect.height;
        }
        private void DeleteSelected()
        {
            foreach (var record in selectedRecords)
            {
                Source.Remove(record);
                ReformCacheUtil.RemoveRecord(record);
            }
            selectedRecords.Clear();
        }
        private float CalculateInitialHeightForRecords()
        {
            return DeleteButtonHeight;
        }
        private void DrawRecords(ref Rect rect)
        {
            var validRecords = GetAvaliableRecords().ToList();

            foreach (var record in validRecords)
            {
                if(record == null)
                {
                    ReformCacheUtil.RemoveRecord(record);
                    continue;
                }
                DrawRecord(record, ref rect);
            }
        }

        private void DrawRecord(ReformRecord record, ref Rect rect)
        {
            var halfWidth = rect.width / 2;
            bool isSelected = selectedRecords.Contains(record);
            Widgets.Checkbox(rect.x, rect.y + rect.height, ref isSelected, CheckboxSize);

            if (isSelected != selectedRecords.Contains(record))
            {
                if (isSelected)
                    selectedRecords.Add(record);
                else
                    selectedRecords.Remove(record);
            }

            Rect leftHalf = new Rect(rect.x + CheckboxSize + 5, rect.y + rect.height, halfWidth, RecordRowHeight);
            Rect rightHalf = new Rect(rect.x + halfWidth + CheckboxSize + 5, rect.y + rect.height, halfWidth, RecordRowHeight);

            var name = record?.Pawn?.Label ?? "Broken pawn record";
            Widgets.Label(leftHalf, name);


            //Status
            if (Source != null)
            {
                if (GlobalReformTracker.IsInQueue(record.Pawn))
                {
                    Widgets.Label(rightHalf, StringKeys.ReformInQueue.Translate());
                }
                else if (GlobalReformTracker.IsReforming(record))
                {
                    Widgets.Label(rightHalf, StringKeys.ReformInProgress.Translate());
                }
            }
            rect.height += RecordRowHeight;
        }

        private IEnumerable<ReformRecord> GetAvaliableRecords()
        {
            return Source.CachedReformRecords;
        }
    }
}
