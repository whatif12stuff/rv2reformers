﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RV2_Reformers.ModCompatiblity.VE
{
    [DefOf]
    public static class VE_Defs
    {
        [MayRequire(VE_Social.PackageID)]
        public static ThoughtDef VSIE_KilledMyBestFriend;
        [MayRequire(VE_Social.PackageID)]
        public static ThoughtDef VSIE_MyBestFriendDied;
        [MayRequire(VE_Social.PackageID)]
        public static ThoughtDef VSIE_MyBestFriendDied_Female;
    }
    public static class VE_Social
    {
        public const string PackageID = "vanillaexpanded.vanillasocialinteractionsexpanded";
    }
}
