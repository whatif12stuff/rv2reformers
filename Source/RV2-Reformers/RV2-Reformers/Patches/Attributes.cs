﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RV2_Reformers.Patches
{
    public class PatchRequiresMod : Attribute
    {
        public string Mod { get; private set; }
        public PatchRequiresMod(string mod)
        {
            this.Mod = mod;
        }
    }
}
