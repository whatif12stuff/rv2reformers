﻿using HarmonyLib;
using RimVore2;
using RimWorld.Planet;
using RV2_Reformers.Settings;
using RV2_Reformers.Debug;
using RV2_Reformers.Reform;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace RV2_Reformers.Patches
{
    [HarmonyPatch(typeof(Pawn_HealthTracker), nameof(Pawn_HealthTracker.NotifyPlayerOfKilled))]
    internal class PlayerDeathNotification
    {
        [HarmonyPrefix]
        public static bool Patch(Pawn_HealthTracker __instance, DamageInfo? dinfo, Hediff hediff, Caravan caravan)
        {
            if (!GetSetting.PatchPlayerDeathNotification) return true;
            try
            {
                Pawn pawn = AccessTools.Field(typeof(Pawn_HealthTracker), "pawn").GetValue(__instance) as Pawn;
                return !GlobalReformTracker.IsReforming(pawn);
            }
            catch (Exception e)
            {
                RV2Log.Error($"Exception catching player death notification {e}", LogCategories.ReformPatches);
            }
            return true;
        }
    }
}
