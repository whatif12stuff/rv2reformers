﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RV2_Reformers.Defs
{
    [DefOf]
    public static class Reformer_ThoughtDefOfs
    {
        public static ThoughtDef Rv2Reformers_PreyReformedPrematuraly;
        public static ThoughtDef Rv2Reformers_Reformed;
    }
}
