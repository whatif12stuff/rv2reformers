﻿using RV2_Reformers.Comps;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace RV2_Reformers.Reform
{
    public static class ReformerFinder
    {
        public static IEnumerable<Comp_Reformer> PlayerOwnedReformersOnMap(Map map)
        {
            if (map == null) return new List<Comp_Reformer>();
            var AllBuildings = map.listerBuildings.allBuildingsColonist;

            return AllBuildings.Select(b => b.TryGetComp<Comp_Reformer>()).Where(b => b != null);
        }
        public static IEnumerable<Comp_Reformer> PlayerOwnedGlobalReformers()
        {
            foreach (Map map in Find.Maps)
            {
                foreach (var reformer in PlayerOwnedReformersOnMap(map).Where(b => b.HasGlobalRange))
                {
                    yield return reformer;
                }
            }
        }
        public static IEnumerable<Comp_Reformer> AllPlayerReformers()
        {
            foreach(var map in Find.Maps)
            {
                foreach(var reformer in PlayerOwnedReformersOnMap(map)) {
                    yield return reformer;
                }
            }
        }
    }
}
