﻿using RV2_Reformers.ModCompatiblity;
using RV2_Reformers.ModCompatiblity.VE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace RV2_Reformers.Utilities
{
    public static class ModsCheckerUtil
    {
        public static bool ModActive(IEnumerable<string> identifiers)
        {
            foreach(var id in identifiers)
            {
                if(ModActive(id)) return true;
            }
            return false;
        }
        public static bool ModActive(string identifier)
        {
            return ModsConfig.ActiveModsInLoadOrder.Any(m=>m.packageIdLowerCase == identifier);
        }
        public static bool IsVE_SocialActive => ModActive(VE_Social.PackageID);
        public static bool IsPawnmorpherActive => ModActive(Pawnmorpher.PackageIds);
        public static bool IsImmortalsActive => ModActive(ReformerSupportedMods.ImmortalsMod);
    }
}
