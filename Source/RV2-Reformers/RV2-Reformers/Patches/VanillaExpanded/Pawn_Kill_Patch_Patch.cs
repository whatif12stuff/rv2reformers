﻿using HarmonyLib;
using RimVore2;
using RV2_Reformers.Debug;
using RV2_Reformers.ModCompatiblity;
using RV2_Reformers.Reform;
using RV2_Reformers.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VanillaSocialInteractionsExpanded;
using Verse;

namespace RV2_Reformers.Patches.VanillaExpanded
{
    
    [PatchRequiresMod(ReformerSupportedMods.VanillaSocialInteractionsExpanded)]
    public static class Pawn_Kill_Patch_Patch
    {
        [HarmonyPatch(typeof(Pawn_Kill_Patch), "Prefix")]
        [HarmonyPrefix]
        public static bool Prefix(Pawn __instance, DamageInfo? dinfo, Hediff exactCulprit = null)
        {
            try
            {
                if (!GetSetting.PatchPlayerDeathNotification) return true;
                return !GlobalReformTracker.IsReforming(__instance);
            }
            catch (Exception e)
            {
                RV2Log.Error("Exception caught in Pawn_Kill_Patch_Patch " + e, LogCategories.ReformPatches);
            }
            return true;
        }
    }
    
}
