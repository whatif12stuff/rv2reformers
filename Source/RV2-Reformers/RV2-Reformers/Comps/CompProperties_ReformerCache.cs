﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace RV2_Reformers.Comps
{
    public class CompProperties_ReformerCache : CompProperties
    {
        public bool IsGlobalReciver = false;
        public int MaxCacheSize = 5;

        public CompProperties_ReformerCache()
        {
            compClass = typeof(Comp_ReformerCache);
        }
    }
}
