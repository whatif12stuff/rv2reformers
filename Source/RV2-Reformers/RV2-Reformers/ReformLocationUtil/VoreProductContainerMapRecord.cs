﻿using RimVore2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace RV2_Reformers.ReformLocationUtil
{
    public class VoreProductContainerMapRecord : IExposable
    {
        public List<Pawn> pawns = new List<Pawn>();
        public VoreProductContainer container;
        public void ExposeData()
        {
            Scribe_Collections.Look(ref pawns, "pawns", LookMode.Reference);
            Scribe_References.Look(ref container, "container");
        }
    }
}
