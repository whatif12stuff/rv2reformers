﻿#if !(v1_4 || v1_3)
using LudeonTK;
#endif
using RimWorld;
using RV2_Reformers.Cacher;
using RV2_Reformers.Defs;
using RV2_Reformers.Reform;
using RV2_Reformers.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Profiling;
using Verse;

namespace RV2_Reformers.Dev
{
    public static class DebugWindowReformRecordOverview
    {
        [DebugAction("Rv2Reformers", "Reform Record Overview", actionType = DebugActionType.Action, allowedGameStates = AllowedGameStates.PlayingOnMap)]
        public static void OpenKnowledgeDebugWindow()
        {
            Find.WindowStack.Add(new Window_ReformRecordOverview());
        }
    }

    public class Window_ReformRecordOverview : Window
    {
        private List<OverviewRecord> Records = new List<OverviewRecord>();
        private bool RecordsDirty;

        private Vector2 scrollPosition;
        private Rect viewRect = new Rect(0, 0, 750, 500);

        public Window_ReformRecordOverview() {
            this.doCloseX = true;
            this.draggable = true;
        }

        public override void PreOpen()
        {
            base.PreOpen();
            RecordsDirty = true;
    }

        private void RebuildOverviewRecords()
        {
            Records.Clear();
            Records = GlobalReformTracker.GetAllRecords().Select(r=>new OverviewRecord(r)).ToList();
            RecordsDirty = false;
        }
        public override void PostClose()
        {
            base.PostClose();
            Records.Clear();
        }

        public override void DoWindowContents(Rect inRect)
        {
            if (RecordsDirty) RebuildOverviewRecords();
            float rowHeight = 24f;
            float buttonHeight = 30f;
            float contentWidth = inRect.width - 16; // scrollbar width

            Widgets.BeginScrollView(new Rect(0, 0, inRect.width, inRect.height), ref scrollPosition, viewRect);


            Listing_Standard listing = new Listing_Standard();
            listing.Begin(viewRect);
            float contentHeight = 0;
            try
            {
                listing.GapLine();

                foreach (var record in Records)
                {
                    var textinfo = $"{record.Prey?.Label}".PadRight(50) + $"{record.Pred?.Label}".PadRight(50) + $"Cached:{record.IsCached}".PadRight(50) + $"Time Till Reform:{record.TicksToReformRemaining}".PadRight(50) + $"HasChip:{record.HasChip}";
                    listing.Label(textinfo);
                    var labelHeight = Text.CalcHeight(textinfo, viewRect.width)*2;
                    var btnCount = 4;

                    Rect buttonRect1 = listing.GetRect(buttonHeight);
                    if (Widgets.ButtonText(new Rect(buttonRect1.x, buttonRect1.y, contentWidth / 4 - 4, buttonRect1.height), "ReCache"))
                    {
                        var reformRecord = GlobalReformTracker.GetRecord(record.Prey);
                        ReformCacheUtil.RemoveRecord(reformRecord);
                        ReformCacheUtil.AddForNewRecord(reformRecord);
                        RecordsDirty = true;
                    }

                    Rect buttonRect2 = new Rect(buttonRect1.x + contentWidth / btnCount + 4, buttonRect1.y, contentWidth / btnCount - 4, buttonRect1.height);
                    Rect buttonRect3 = new Rect(buttonRect2.x + contentWidth / btnCount + 4, buttonRect1.y, contentWidth / btnCount - 4, buttonRect1.height);
                    Rect buttonRect4 = new Rect(buttonRect3.x + contentWidth / btnCount + 4, buttonRect1.y, contentWidth / btnCount - 4, buttonRect1.height);
                    if (Widgets.ButtonText(buttonRect2, "Delete"))
                    {
                        GlobalReformTracker.UnregisterRecord(record.Prey);
                        RecordsDirty = true;
                    }
                    if (Widgets.ButtonText(buttonRect3, "Schedule Reform"))
                    {
                        var reformRecord = GlobalReformTracker.GetRecord(record.Prey);
                        AutoReformUtil.DoTimedReform(reformRecord);
                        RecordsDirty = true;
                    }
                    if (Widgets.ButtonText(buttonRect4, "Reform At"))
                    {
                        var targetParam = new TargetingParameters();
                        targetParam.canTargetLocations = true;
                        Find.Targeter.BeginTargeting(targetParam, (LocalTargetInfo target) =>
                        {
                            var reformRecord = GlobalReformTracker.GetRecord(record.Prey);
                            ReformUtil.TryReformPawn(reformRecord, Find.CurrentMap, target.Cell);
                            RecordsDirty = true;
                        });
                    }
                    listing.Gap(buttonHeight / 2);
                    contentHeight += labelHeight + buttonHeight;
                }
                viewRect.height = contentHeight + 100;
            }
            catch (Exception e)
            {
                Widgets.Label(inRect, $"Exception rendering UI: {e}");
            }

            listing.End();
            Widgets.EndScrollView();
        }

        public override Vector2 InitialSize
        {
            get { return new Vector2(800, 500); }
        }
    }
    internal class OverviewRecord
    {
        public bool IsCached;
        public Pawn Pred;
        public Pawn Prey;
        public int TicksToReformRemaining;
        public bool HasChip;
        public OverviewRecord(ReformRecord record)
        {
            Pred = record.Pred;
            Prey = record.Pawn;
            IsCached = ReformCacheUtil.GetCacheForRecord(record) != null;
            var schedule = Rv2ReformersFind.ScheduleReformationManager.GetSchedule(Prey);
            TicksToReformRemaining = schedule == null ? -1 : schedule.TicksTillReform;
            HasChip = Prey.health?.hediffSet != null && Prey.health.hediffSet.HasHediff(Reformers_HediffDefOfs.ReformChipHediff);
        }
    }
}
