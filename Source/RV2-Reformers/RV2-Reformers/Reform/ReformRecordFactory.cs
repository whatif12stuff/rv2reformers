﻿using HarmonyLib;
using RimVore2;
using RimWorld;
using RV2_Reformers.Debug;
using RV2_Reformers.Defs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace RV2_Reformers.Reform
{
    public static class ReformRecordFactory
    {
        public static ReformRecord MakeAndRegisterRecord(VoreTrackerRecord record)
        {
            var reformRecord = MakeRecord(record);
            GlobalReformTracker.RegisterRecord(reformRecord);
            return reformRecord;
        }
        public static ReformRecord MakeRecord(VoreTrackerRecord record)
        {
            ReformRecord reformRecord = new ReformRecord()
            {
                Pawn = record.Prey,
                Pred = record.Predator,
                Map = record.TopPredator?.Map,
                CreatedTick = GenTicks.TicksAbs,
                Importance = GetImportanceOfPawn(record.Prey),
                workSettings = record.Prey.workSettings,
                HasReformChip = record.Prey?.health?.hediffSet?.HasHediff(Reformers_HediffDefOfs.ReformChipHediff) == true
            };

            SetupThoughts(reformRecord);

            return reformRecord;
        }


        //todo replace this hardcoded system with a better one
        //Maybe have a default calcuated one but allow player to change record importance
        private const float ColonistImportance = 100;
        private const float SlaveImportance = 75;
        private const float PrisonerImportance = 50;
        private const float ColonistAnimalImportance = 60;
        private const float NeutralImportance = 50;
        private const float UnknownImportance = 25;
        private const float AnimalImportance = 0;
        private const float HostileImportance = 0;
        private static float GetImportanceOfPawn(Pawn prey)
        {
            if (prey.IsAnimal() && prey.Faction.IsPlayerSafe()) return ColonistAnimalImportance;
            if (prey.IsFreeNonSlaveColonist) return ColonistImportance;
            if (prey.IsSlaveOfColony) return SlaveImportance;
            if (prey.IsPrisonerOfColony) return PrisonerImportance;
            if (prey.Faction != null)
            {
                if (prey.Faction.HostileTo(Find.FactionManager.OfPlayer)) return HostileImportance;
                if (prey.Faction.AllyOrNeutralTo(Find.FactionManager.OfPlayer)) return NeutralImportance;
            }
            if (prey.IsAnimal()) return AnimalImportance;
            return UnknownImportance;
        }

        private static void SetupThoughts(ReformRecord record)
        {
            ThoughtHandler thoughts = record.Pawn?.needs?.mood?.thoughts;
            if (thoughts == null) return;
            SetupSituationalThoughts(record, thoughts);
            SetupMemories(record, thoughts);
        }

        private static void SetupSituationalThoughts(ReformRecord Record, ThoughtHandler thoughts)
        {
            try
            {
                SituationalThoughtHandler situationalHandler = thoughts.situational;
                FieldInfo field = AccessTools.Field(typeof(SituationalThoughtHandler), "cachedThoughts");
                if (field == null)
                {
                    RV2Log.Error("Situational Thought CachedThoughts is null.", LogCategories.ReformRecordFactory);
                    return;
                }
                if (!(field.GetValue(situationalHandler) is List<Thought_Situational> situational))
                {
                    RV2Log.Error("Cached Thoughts was null.", LogCategories.ReformRecordFactory);
                    return;
                }
                Record.CachedSituationals = situational;
            }
            catch (Exception e)
            {
                RV2Log.Error($"Exception while trying to cache situational thoughts.\n{e}", LogCategories.ReformRecordFactory);
            }
        }

        private static void SetupMemories(ReformRecord record, ThoughtHandler thoughts)
        {
            try
            {
                record.CachedMemories = thoughts.memories.Memories;
            }
            catch (Exception e)
            {
                RV2Log.Error($"Exception while trying to cache situational thoughts.\n{e}", LogCategories.ReformRecordFactory);
            }
        }
    }
}
