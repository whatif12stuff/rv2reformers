﻿using HarmonyLib;
using RimVore2;
using RV2_Reformers.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace RV2_Reformers.Patches.RV2
{
    [HarmonyPatch(typeof(VoreInteraction), "CalculateValidity")]
    internal class VoreInteractionPatch
    {
        [HarmonyPostfix]
        public static void Patch(ref VoreInteraction __instance)
        {
            if(__instance.Prey?.GetVoreRecord() != null)
            {
                __instance.InteractionInvalidReason = StringKeys.InvalidDueToExistingRecordKey.Translate();
            }
        }
    }
}
